﻿/// File for saving data to DB

var DBHandler = function () {
    var self = this;
    var AnimalModel = require('./DatabaseModel/AnimalModel.js');
    var RssModel = require('./DatabaseModel/RssModel.js');
    var ActionsModel = require('./DatabaseModel/ActionsModel.js');
    var VersionModel = require('./DatabaseModel/VersionsModel.js');
    var request = require('request');
    var sharp = require('sharp');

    var versionId = "v1";

    // check if RSS item is in DB, then create or update 
    self.saveRss = function (data, date) {
        var regexp = new RegExp("<[^>]*>", 'g');
        var nbspregexp = new RegExp("(&nbsp;)", 'g'); 
        RssModel.findOne({ $and: [{ title: data.title }, { date: data.pubDate }, { link: data.link }, { description: data.content.replace(regexp, "") }] },
            function (error, doc) {
                if (error) console.log(error);
                else {
                    if (doc == null) {
                        console.log(data.title + " " + data.pubDate)
                        var rss = new RssModel({
                            title: data.title,
                            link: data.link,
                            description: data.content.replace(regexp, "").replace(nbspregexp, "\u00A0"),
                            date: data.pubDate,
                            updated_at: date,
                            last_visit: date,
                            deleted: false
                        });
                        VersionModel.findByIdAndUpdate("v1", { rssUpdatedAt: date }, { upsert: true }, function (err, obj) {
                            if (err || obj == null) console.log(err);
                        });
                        rss.save();
                    }
                    else {
                        doc.last_visit = date;
                        doc.deleted = false;
                        doc.save();
                    }
                }
            });
    }

    // check if action item is in DB, then create or update     
    self.saveAction = function (data, date) {
        var regexp = new RegExp("<[^>]*>", 'g');      
        var nbspregexp = new RegExp("(&nbsp;)", 'g');   
        var urlString = "";
        var url = data.description.match("href=\"[^\"]*\"");
        // parse url from description
        if (url != null && (url[0].match(new RegExp("mailto")) == null))
        {
            if ((url[0].match(new RegExp("https://")) == null) && (url[0].match(new RegExp("http://")) == null))
                urlString = "https://www.zoopraha.cz/" + url[0].substring(6, url[0].length - 1);
            else 
                urlString = url[0].substring(6, url[0].length - 1);
        }
        ActionsModel.findOne({ $and: [{ summary: data.summary }, { start: data.start }, { end: data.end }, { description: data.description.replace(regexp, "") }, { url: urlString }] },
            function (error, doc) {
                if (error) console.log(error);
                else {
                    var updateNeed = false;

                    if (doc == null) {
                        var action = new ActionsModel({
                            start: data.start,
                            end: data.end,
                            summary: data.summary,
                            description: data.description.replace(regexp, "").replace(nbspregexp, "\u00A0"), // remove html tags
                            url: urlString,
                            updated_at: date,
                            last_visit: date,
                            deleted: false
                        });
                        VersionModel.findByIdAndUpdate("v1", { actionUpdatedAt: date }, { upsert: true }, function (err, obj) {
                            if (err || obj == null) console.log(err);
                        });
                        action.save();
                    }
                    else {
                        doc.last_visit = date;
                        doc.deleted = false;
                        doc.save();
                    }                  
                }

            });

    }

    // mark action which was not visited (not in open data) as deleted
    self.markAsDeletedAction = function (date) {
        var day = date.getDate() - 1;
        var month = date.getMonth();
        var year = date.getFullYear();
        if (day < 1) {
            month--;
            day = 28;
            if (month < 1) {
                month = 12;
                year--;
            }
        }
        var d = new Date(year, month, day, 0, 0, 0);
        ActionsModel.find({ last_visit: { $lt: d } }, function (err, docs) {
            //console.log("del");
            if (docs != null)
                docs.forEach(function (entry) {
                    //console.log(entry);
                    entry.deleted = true;
                    entry.updated_at = date;
                    entry.save();
                });
            VersionModel.findByIdAndUpdate("v1", { rssUpdatedAt: date }, { upsert: true }, function (err, obj) {
                if (err || obj == null) console.log(err);
            });
        });
    }

    // mark RSS which was not visited (not in open data) as deleted
    self.markAsDeletedRss = function (date) {
        var day = date.getDate() - 1;
        var month = date.getMonth();
        var year = date.getFullYear();
        if (day < 1) {
            month--;
            day = 28;
            if (month < 1) {
                month = 12;
                year--;
            }
        }
        var d = new Date(year, month, day, 0, 0, 0);
        RssModel.find({ last_visit: { $lt: d } }, function (err, docs) {            
            if (docs != null)
                docs.forEach(function (entry) {                    
                    entry.deleted = true;
                    entry.updated_at = date;
                    entry.save();
                });
            VersionModel.findByIdAndUpdate("v1", { actionUpdatedAt: date }, { upsert: true }, function (err, obj) {
                if (err || obj == null) console.log(err);
            });
        });
    }

    // mark animals which was not visited (not in open data) as deleted
    self.markAsDeletedAnimals = function (date) {
        var day = date.getDate() - 1;
        var month = date.getMonth();
        var year = date.getFullYear();
        if (day < 1) {
            month--;
            day = 28;
            if (month < 1) {
                month = 12;
                year--;
            }
        }
        var d = new Date(year, month, day, 0, 0, 0);
        AnimalModel.find({ last_visit: { $lt: d } }, function (err, docs) {            
            if (docs != null)
                docs.forEach(function (entry) {
                    entry.deleted = true;
                    entry.updated_at = date;
                    entry.save();
                });
            VersionModel.findByIdAndUpdate("v1", { animalUpdatedAt: date }, { upsert: true }, function (err, obj) {
                if (err || obj == null) console.log(err);
            });
        });
    }

    // Insert/update animal to DB
    self.saveAnimal = function (animal, date) {
        if (animal != null) {
            var htmlregexp = new RegExp("<[^>]*>", 'g');
            var nbspregexp = new RegExp("(&nbsp;)", 'g');            
            var imageregexp = /images[^\\]*jpg/i; 
            var imgurlArr = animal.description.match(imageregexp);
            var imgurl = "";
            if (imgurlArr != null) imgurl = "https://zoopraha.cz/" + imgurlArr[0];
            var animalModel = new AnimalModel({
                _id: animal.id,
                Title: animal.title,
                LatinTitle: animal.latin_title,
                ImageAlt: "",
                ImageUrl: imgurl,
                Continent: animal.continents != null ? animal.continents.name_c : "",
                Class: animal.classes.title,
                Order: animal.order.title,
                spreadNote: animal.spread_note,
                Biotop: animal.biotop != null ? animal.biotop.name_b : "",
                BiotopesNotes: animal.biotopes_note,
                Food: animal.food != null ? animal.food.name_f : "",
                FoodNotes: animal.food_note,
                Proportions: animal.proportions,
                Reproduction: animal.reproduction,
                Attractions: animal.attractions.replace(htmlregexp, "").replace(nbspregexp, "\u00A0"),
                ProjectsNote: animal.projects_note,
                Breeding: animal.breeding.replace(htmlregexp, "").replace(nbspregexp, "\u00A0"),
                LocalitiesTitle: animal.localities != null ? animal.localities.title : "",
                LocalitiesUrl: animal.localities != null ? animal.localities.url : "",
                Description: animal.description.replace(htmlregexp, "").replace(nbspregexp, "\u00A0")
            });

            AnimalModel.findById(animal.id,
                function (error, doc) {
                    if (error) console.log(error);
                    else {
                        var updateNeed = false;

                        if (doc == null ||
                            doc.Title != animalModel.Title ||
                            doc.LatinTitle != animalModel.LatinTitle ||
                            doc.ImageUrl != animalModel.ImageUrl ||
                            doc.Continent != animalModel.Continent ||
                            doc.Class != animalModel.Class ||
                            doc.Order != animalModel.Order ||
                            doc.spreadNote != animalModel.spreadNote ||
                            doc.Biotop != animalModel.Biotop ||
                            doc.BiotopesNotes != animalModel.BiotopesNotes ||
                            doc.Food != animalModel.Food ||
                            doc.FoodNotes != animalModel.FoodNotes ||
                            doc.Proportions != animalModel.Proportions ||
                            doc.Reproduction != animalModel.Reproduction ||
                            doc.Attractions != animalModel.Attractions ||
                            doc.ProjectsNote != animalModel.ProjectsNote ||
                            doc.Breeding != animalModel.Breeding ||
                            doc.LocalitiesTitle != animalModel.LocalitiesTitle ||
                            doc.LocalitiesUrl != animalModel.LocalitiesUrl ||
                            doc.Description != animalModel.Description
                        ) updateNeed = true;

                        if (updateNeed) {                            
                            if (doc == null) doc = new AnimalModel({
                                _id: animal.id,
                                Title: animal.title,
                                LatinTitle: animal.latin_title,
                                ImageAlt: "",//
                                ImageUrl: imgurl,
                                Continent: animal.continents != null ? animal.continents.name_c : "",
                                Class: animal.classes.title,
                                Order: animal.order.title,
                                spreadNote: animal.spread_note,
                                Biotop: animal.biotop != null ? animal.biotop.name_b : "",
                                BiotopesNotes: animal.biotopes_note,
                                Food: animal.food != null ? animal.food.name_f : "",
                                FoodNotes: animal.food_note,
                                Proportions: animal.proportions,
                                Reproduction: animal.reproduction,
                                Attractions: animal.attractions.replace(htmlregexp, " "),
                                ProjectsNote: animal.projects_note,
                                Breeding: animal.breeding.replace(htmlregexp, " "),
                                LocalitiesTitle: animal.localities != null ? animal.localities.title : "",
                                LocalitiesUrl: animal.localities != null ? animal.localities.url : "",
                                Description: animal.description.replace(htmlregexp, " "),
                            });
                            else {
                                doc.Title = animalModel.Title;
                                doc.LatinTitle = animalModel.LatinTitle;
                                doc.ImageUrl = animalModel.ImageUrl;
                                doc.Continent = animalModel.Continent;
                                doc.Class = animalModel.Class;
                                doc.Order = animalModel.Order;
                                doc.spreadNote = animalModel.spreadNote;
                                doc.Biotop = animalModel.Biotop;
                                doc.BiotopesNotes = animalModel.BiotopesNotes;
                                doc.Food = animalModel.Food;
                                doc.FoodNotes = animalModel.FoodNotes;
                                doc.Proportions = animalModel.Proportions;
                                doc.Reproduction = animalModel.Reproduction;
                                doc.Attractions = animalModel.Attractions;
                                doc.ProjectsNote = animalModel.ProjectsNote;
                                doc.Breeding = animalModel.Breeding;
                                doc.LocalitiesTitle = animalModel.LocalitiesTitle;
                                doc.Description = animalModel.Description;
                                doc.LocalitiesUrl = animalModel.LocalitiesUrl;
                            }
                            doc.updated_at = date;
                            VersionModel.findByIdAndUpdate("v1", { animalUpdatedAt: date }, { upsert: true }, function (err, obj) {
                                if (err || obj == null) console.log(err);
                            });
                        }
                        doc.last_visit = date;
                        var url = encodeURI(doc.ImageUrl);
                        if (url != null && url != "")
                            request
                                .get({ url: url, encoding: null, forever: true, timeout: 120000 })
                                .on('error', function (err) {
                                    
                                })
                                .on('response', function (response) {
                                })
                                .on('end', (resp, bodyBuffer) => {                                    
                                                          
                                    if (bodyBuffer != null) {
                                        sharp(bodyBuffer)
                                            .resize(300, 200)
                                            .embed()
                                            .toBuffer(function (err, buffer, info) {
                                                doc.image = buffer;
                                                doc.save();
                                            });
                                    }
                                });
                        doc.deleted = false;
                        doc.save();
                    }
                });
        }
    }
}

module.exports = DBHandler;
