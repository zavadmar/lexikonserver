﻿var mongoose = require('mongoose');

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open ');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

var Schema = mongoose.Schema;

var VersionSchema = new Schema({
    _id: String,
    animalUpdatedAt: Date,
    actionUpdatedAt: Date,
    rssUpdatedAt: Date
});

module.exports = mongoose.model('Version', VersionSchema);
