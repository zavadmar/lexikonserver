﻿var mongoose = require('mongoose');

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open ');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

var Schema = mongoose.Schema;

var actionsSchema = new Schema({
    start: Date,
    end: Date,
    summary: String,
    description: String,
    url: String,    
    updated_at: Date,
    last_visit: Date,
    deleted: Boolean
});

module.exports = mongoose.model('Actions', actionsSchema);