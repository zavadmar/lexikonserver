﻿/// File for download data from Zoo OpenData

var getZooData = function () {
    var self = this;
    var http = require('http');

    // Download RSS
    self.getRss = function () {
        var url = "https://www.zoopraha.cz/?format=feed&type=rss";
        var parser = require('rss-parser');
        parser.parseURL(url, function (err, parsed) {
            var handler = require('./DBHandler.js');
            var dbHandler = new handler();
            var date = new Date();
            var cnt = 0;
            if (parsed.feed.entries.length > 0)
                parsed.feed.entries.forEach(function (entry) {
                    dbHandler.saveRss(entry, date);
                    cnt++;
                    if (cnt >= parsed.feed.entries.length) {
                        dbHandler.markAsDeletedRss(date);
                    }
                });
        });
    }

    // Download actions
    self.getActions = function () {
        var url = "https://www.zoopraha.cz/_api/json/?action=akcezoo&type=rel";
        var http = require('https');
        http.get(url, function (res) {
            var body = '';
            res.on('data', function (data) {
                body += data;
            });
            res.on('end', function () {
                body = "{ \"data\":" + body.substring(1) + "}";
                var parsed = JSON.parse(body);
                var handler = require('./DBHandler.js');
                var dbHandler = new handler();
                var date = new Date();
                var cnt = 0;
                if (parsed.data.length > 0)
                    parsed.data.forEach(function (action) {
                        dbHandler.saveAction(action, date);
                        cnt++;
                        if (cnt >= parsed.data.length) {
                            dbHandler.markAsDeletedAction(date);
                        }
                    });
            });
        });
    }

    // Download animals
    self.getLexicon = function () {
        var http = require('https');
        var newUrl = "https://www.zoopraha.cz/_api/json/?action=lexikoncelek&type=rel";
        http.get(newUrl, function (res) {
            var body = '';
            res.on('data', function (data) {
                body += data;
            });
            res.on('end', function () {
                body = "{ \"data\":" + body.substring(1) + "}";  // substring is for remove first invisible character
                var parsed = JSON.parse(body);
                var handler = require('./DBHandler.js');
                var dbHandler = new handler();
                var date = new Date();
                var cnt = 0;
                if (parsed.data.length > 0)
                    parsed.data.forEach(function (animal) {
                        dbHandler.saveAnimal(animal, date);
                        cnt++;
                        if (cnt >= parsed.data.length) {
                            dbHandler.markAsDeletedAnimals(date);
                        }
                    });
                console.log("OK");             
            });
        });
    }
}

module.exports = getZooData;