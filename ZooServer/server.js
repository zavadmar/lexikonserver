﻿//git: https://git.heroku.com/agile-chamber-32011.git
var http = require('http');
var port = process.env.PORT || 1337;
var request = require('request');
// Download data
var getData = require('./GetData.js')
var getDataI = new getData();
getDataI.getRss();
getDataI.getActions();
getDataI.getLexicon();

//set interval for repeated download
setInterval(getDataI.getLexicon, 1000 * 60 * 60 * 24);
setInterval(getDataI.getRss, 1000 * 60 * 60 * 24);
setInterval(getDataI.getActions, 1000 * 60 * 60 * 24);

var express = require('express');
var app = express();
var router = express.Router();

// DB models
var animalModel = require('./DatabaseModel/AnimalModel.js');
var rssModel = require('./DatabaseModel/RssModel.js');
var actionsModel = require('./DatabaseModel/ActionsModel.js');
var versionModel = require('./DatabaseModel/VersionsModel.js');

var versionId = "v1";

// Get all animals

router.route('/animals').get(function (req, res) {
    console.log("GET Animals");
    var since = new Date(1970, 1, 1);
    if (req.headers['if-modified-since'])
        since = Date.parse(req.headers['if-modified-since'].toString());
    console.log(since);
    animalModel.find({ updated_at: { $gt: since } }, { image: 0 }, function (err, animals) {
        if (err) {
            res.send(err);
        }
        if (animals == null || animals[0] == null) {
            res.status(304).send("Not modified");        
        }
        else versionModel.findById(versionId, function (err, version) {
            if (err)
                res.send(err);
            else {
                res.setHeader('Last-Modified', version.animalUpdatedAt.toUTCString());
                res.json(animals);
            }
        });
    });
});

// Get all rss
router.route('/rss').get(function (req, res) {
    console.log("GET Rss");

    var since = new Date(1970, 1, 1);
    if (req.headers['if-modified-since'])
        since = new Date(req.headers['if-modified-since'].toString());
    since = new Date(since.getTime() + 1000);
    rssModel.find({ updated_at: { $gt: since } }, function (err, rss) {
        if (err)
            res.send(err);
        if (rss == null || rss[0] == null) {
            res.status(304).send("Not modified");
        }
        else versionModel.findById(versionId, function (err, version) {
            if (err)
                res.send(err);
            else {
                res.setHeader('Last-Modified', version.rssUpdatedAt.toUTCString());
                res.json(rss);
            }
        });
    });
});

// Get all actions
router.route('/actions').get(function (req, res) {
    console.log("GET Actions");
    var since = new Date(1970, 1, 1);
    if (req.headers['if-modified-since'])
        since = new Date(req.headers['if-modified-since'].toString());
    since = new Date(since.getTime() + 1000);
    actionsModel.find({ updated_at: { $gt: since } }, function (err, rss) {
        if (err)
            res.send(err);
        if (rss == null || rss[0] == null) {
            res.status(304).send("Not modified");
        }
        else versionModel.findById(versionId, function (err, version) {
            if (err)
                res.send(err);
            else {
                res.setHeader('Last-Modified', version.actionUpdatedAt.toUTCString());
                res.json(rss);
            }
        });
    });

});

// get image of animal by animal id
router.route('/animals/images/:id').get(function (req, res) {
    animalModel.findById(req.params.id, function (err, animal) {
        if (err) {
            res.send(err);
            console.log("err")
        }
        if (animal != null) {
            var url = encodeURI(animal.ImageUrl);
            res.set('Content-Type', 'image/jpg');
            res.end(animal.image, 'binary');
        }
        else {
            res.status(404).send("Not found");
        }
    });
});

//create api
app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);
