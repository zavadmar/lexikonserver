﻿    var mongoose = require('mongoose');
    mongoose.connect('mongodb://martin:mojeheslo@ds143181.mlab.com:43181/heroku_gt0r90wn');

    mongoose.connection.on('connected', function () {
        console.log('Mongoose default connection open ');
    });

    // If the connection throws an error
    mongoose.connection.on('error', function (err) {
        console.log('Mongoose default connection error: ' + err);
    });

    // When the connection is disconnected
    mongoose.connection.on('disconnected', function () {
        console.log('Mongoose default connection disconnected');
    });

    var Schema = mongoose.Schema;

    var animalSchema = new Schema({
        _id: { type: Number, required: true, unique: true },
        //alias: String,
        Title: String,
        LatinTitle: String,
        ImageAlt:String,
        ImageUrl: String,
        Continent: String,
        Class: String,
        Order: String,
        spreadNote: String,
        Biotop: String,
        BiotopesNotes: String,
        Food: String,
        FoodNotes: String,
        Proportions: String,
        Reproduction: String,
        Attractions: String,
        ProjectsNote: String,
        Breeding: String,
        LocalitiesTitle: String,
        LocalitiesUrl: String,
        Description: String,

        updated_at: Date,
        last_visit: Date,
        deleted: Boolean,
        image: Buffer
    });
    module.exports = mongoose.model('Animal', animalSchema);